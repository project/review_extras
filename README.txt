UberCart Review Extras

DESCRIPTION

Ubercart Review Extras provides an extra Pane on the Checkout Review page of a UberCart store.

This pane allows for 8 brief (or long) policy statements to be shown to a customer before sending off payment information during the Review process. Some Australia banks and financial institutions, and consumer watchdogs require this information be displayed before granting Merchant accounts to businesses.

The policies by default are named Currency, Domicile, Privacy, Security, Refunds, Returns, Delivery and Product.

Each review policy can be individually renamed and enabled or disabled.

INSTALLATION

Copy the contents of the module package to a folder in your 'sites/all/modules' directory.
Enable the module under '/admin/build/modules'.

CONFIGURATION

All settings can be configured under Ubercart's Checkout Settings at '/admin/store/settings/checkout/.

The global enable function can be used to disable without disabling the module.

The Review Pane itself can have any title you wish set.

Each policy can be renamed and individually disabled.

