<?php

/**
 * @file
 * Provides the UI for Adminstration forms
 */


/**
 * Admin page block.
 */
function uc_review_extras_admin_menu_block_page() {
  $item = menu_get_item();
  if ($content = system_admin_menu_block($item)) {
    $output = theme('admin_block_content', $content);
  }
  else {
    $output = t('You do not have any administrative items.');
  }
  return $output;
}


function uc_review_extras_admin() {


$form = array();

$form['general'] = array(
  '#weight' => '-10',
  '#collapsed' => '0',
  '#title' => t('General Settings'),
  '#collapsible' => '1',
  '#type' => 'fieldset',

);

$form['general']['uc_review_extras_enabled'] = array(
  '#weight' => '0',
  '#type' => 'checkbox',
  '#title' => t('Enable Review Extra'),
  '#default_value' => variable_get('uc_review_extras_enabled', TRUE),
  '#summary callback' => 'summarize_checkbox',
  '#summary arguments' => array(
      t('Review Extras is enabled.'),
      t('Review Extras is not enabled.'),
    ),
);

$form['general']['uc_review_extras_pane_title'] = array(
  '#weight' => '0',
  '#size' => '24',
  '#type' => 'textfield',
  '#title' => t('Extras Pane Title'),
  '#default_value' => variable_get('uc_review_extras_pane_title', 'Policy Information'),
);




//Currency

$form['currency'] = array(
  '#weight' => '-9',
  '#collapsed' => '0',
  '#title' => t(variable_get('uc_review_extras_currency_title', 'Currency')),
  '#collapsible' => '1',
  '#type' => 'fieldset',

);

$form['currency']['uc_review_extras_currency_show'] = array(
  '#weight' => '0',
  '#type' => 'checkbox',
  '#title' => t("Display '@title' notice", array('@title' => variable_get('uc_review_extras_currency_title', ''))),
  '#default_value' => variable_get('uc_review_extras_currency_show', TRUE),

  '#summary callback' => 'summarize_checkbox',
  '#summary arguments' => array(
      t("The '@title' notice is enabled.", array('@title' => variable_get('uc_review_extras_currency_title', ''))),
      t("The '@title' notice is disabled.", array('@title' => variable_get('uc_review_extras_currency_title', ''))),
    ),
);

$form['currency']['uc_review_extras_currency_title'] = array(
  '#weight' => '0',
  '#size' => '24',
  '#type' => 'textfield',
  '#title' => t('Title'),
  '#default_value' => variable_get('uc_review_extras_currency_title', 'Currency'),
);

$form['currency']['uc_review_extras_currency_text'] = array(
  '#rows' => '5',
  '#cols' => '40',
  '#weight' => '1',
  '#type' => 'textarea',
  '#title' => t('Text'),
  '#default_value' => variable_get('uc_review_extras_currency_text', 'Currency note here'),
);



//Domicile

$form['domicile'] = array(
  '#weight' => '-2',
  '#collapsed' => '0',
  '#title' => t(variable_get('uc_review_extras_domicile_title', 'Domicile')),
  '#collapsible' => '1',
  '#type' => 'fieldset',

);

$form['domicile']['uc_review_extras_domicile_show'] = array(
  '#weight' => '0',
  '#type' => 'checkbox',
  '#title' => t("Display '@title' notice", array('@title' => variable_get('uc_review_extras_domicile_title', ''))),
  '#default_value' => variable_get('uc_review_extras_domicile_show', TRUE),

  '#summary callback' => 'summarize_checkbox',
  '#summary arguments' => array(
      t("The '@title' notice is enabled.", array('@title' => variable_get('uc_review_extras_domicile_title', ''))),
      t("The '@title' notice is disabled.", array('@title' => variable_get('uc_review_extras_domicile_title', ''))),
    ),
);

$form['domicile']['uc_review_extras_domicile_title'] = array(
  '#weight' => '0',
  '#size' => '24',
  '#type' => 'textfield',
  '#title' => t('Title'),
  '#default_value' => variable_get('uc_review_extras_domicile_title', 'Domicile'),
);

$form['domicile']['uc_review_extras_domicile_text'] = array(
  '#rows' => '5',
  '#cols' => '40',
  '#weight' => '1',
  '#type' => 'textarea',
  '#title' => t('Text'),
  '#default_value' => variable_get('uc_review_extras_domicile_text', 'Domicile note here'),
);



//Privacy

$form['privacy'] = array(
  '#weight' => '-3',
  '#collapsed' => '0',
  '#title' => t(variable_get('uc_review_extras_privacy_title', 'Privacy')),
  '#collapsible' => '1',
  '#type' => 'fieldset',

);

$form['privacy']['uc_review_extras_privacy_show'] = array(
  '#weight' => '0',
  '#type' => 'checkbox',
  '#title' => t("Display '@title' notice", array('@title' => variable_get('uc_review_extras_privacy_title', ''))),
  '#default_value' => variable_get('uc_review_extras_privacy_show', TRUE),

  '#summary callback' => 'summarize_checkbox',
  '#summary arguments' => array(
      t("The '@title' notice is enabled.", array('@title' => variable_get('uc_review_extras_privacy_title', ''))),
      t("The '@title' notice is disabled.", array('@title' => variable_get('uc_review_extras_privacy_title', ''))),
    ),
);

$form['privacy']['uc_review_extras_privacy_title'] = array(
  '#weight' => '0',
  '#size' => '24',
  '#type' => 'textfield',
  '#title' => t('Title'),
  '#default_value' => variable_get('uc_review_extras_privacy_title', 'Privacy'),
);

$form['privacy']['uc_review_extras_privacy_text'] = array(
  '#rows' => '5',
  '#cols' => '40',
  '#weight' => '1',
  '#type' => 'textarea',
  '#title' => t('Text'),
  '#default_value' => variable_get('uc_review_extras_privacy_text', 'Privacy note here'),
);



//Product

$form['product'] = array(
  '#weight' => '-4',
  '#collapsed' => '0',
  '#title' => t(variable_get('uc_review_extras_product_title', 'Product')),
  '#collapsible' => '1',
  '#type' => 'fieldset',

);

$form['product']['uc_review_extras_product_show'] = array(
  '#weight' => '0',
  '#type' => 'checkbox',
  '#title' => t("Display '@title' notice", array('@title' => variable_get('uc_review_extras_product_title', ''))),
  '#default_value' => variable_get('uc_review_extras_product_show', TRUE),

  '#summary callback' => 'summarize_checkbox',
  '#summary arguments' => array(
      t("The '@title' notice is enabled.", array('@title' => variable_get('uc_review_extras_product_title', ''))),
      t("The '@title' notice is disabled.", array('@title' => variable_get('uc_review_extras_product_title', ''))),
    ),
);

$form['product']['uc_review_extras_product_title'] = array(
  '#weight' => '0',
  '#size' => '24',
  '#type' => 'textfield',
  '#title' => t('Title'),
  '#default_value' => variable_get('uc_review_extras_product_title', 'Product'),
);

$form['product']['uc_review_extras_product_text'] = array(  '#rows' => '5',
  '#cols' => '40',
  '#weight' => '1',
  '#type' => 'textarea',
  '#title' => t('Text'),
  '#default_value' => variable_get('uc_review_extras_product_text', 'Product note here'),
);



//Returns

$form['returns'] = array(
  '#weight' => '-5',
  '#collapsed' => '0',
  '#title' => t(variable_get('uc_review_extras_returns_title', 'Returns')),
  '#collapsible' => '1',
  '#type' => 'fieldset',

);

$form['returns']['uc_review_extras_returns_show'] = array(
  '#weight' => '0',
  '#type' => 'checkbox',
  '#title' => t("Display '@title' notice", array('@title' => variable_get('uc_review_extras_returns_title', ''))),
  '#default_value' => variable_get('uc_review_extras_returns_show', TRUE),

  '#summary callback' => 'summarize_checkbox',
  '#summary arguments' => array(
      t("The '@title' notice is enabled.", array('@title' => variable_get('uc_review_extras_returns_title', ''))),
      t("The '@title' notice is disabled.", array('@title' => variable_get('uc_review_extras_returns_title', ''))),
    ),
);

$form['returns']['uc_review_extras_returns_title'] = array(
  '#weight' => '0',
  '#size' => '24',
  '#type' => 'textfield',
  '#title' => t('Title'),
  '#default_value' => variable_get('uc_review_extras_returns_title', 'Returns'),
);

$form['returns']['uc_review_extras_returns_text'] = array(
  '#rows' => '5',
  '#cols' => '40',
  '#weight' => '1',
  '#type' => 'textarea',
  '#title' => t('Text'),
  '#default_value' => variable_get('uc_review_extras_returns_text', 'Returns note here'),
);



//Refunds

$form['refunds'] = array(
  '#weight' => '-6',
  '#collapsed' => '0',
  '#title' => t(variable_get('uc_review_extras_refunds_title', 'Refunds')),
  '#collapsible' => '1',
  '#type' => 'fieldset',
);

$form['refunds']['uc_review_extras_refunds_show'] = array(
  '#weight' => '0',
  '#type' => 'checkbox',
  '#title' => t("Display '@title' notice", array('@title' => variable_get('uc_review_extras_refunds_title', 'Refunds'))),
  '#default_value' => variable_get('uc_review_extras_refunds_show', TRUE),

  '#summary callback' => 'summarize_checkbox',
  '#summary arguments' => array(
      t("The '@title' notice is enabled.", array('@title' => variable_get('uc_review_extras_refunds_title', 'Refunds'))),
      t("The '@title' notice is disabled.", array('@title' => variable_get('uc_review_extras_refunds_title', 'Refunds'))),
    ),
);

$form['refunds']['uc_review_extras_refunds_title'] = array(
  '#weight' => '0',
  '#size' => '24',
  '#type' => 'textfield',
  '#title' => t('Title'),
  '#default_value' => variable_get('uc_review_extras_refunds_title', 'Refunds'),
);

$form['refunds']['uc_review_extras_refunds_text'] = array(
  '#rows' => '5',
  '#cols' => '40',
  '#weight' => '1',
  '#type' => 'textarea',
  '#title' => t('Text'),
  '#default_value' => variable_get('uc_review_extras_refunds_text', 'Refunds note here'),
);



//Delivery

$form['delivery'] = array(
  '#weight' => '-7',
  '#collapsed' => '0',
  '#title' => t(variable_get('uc_review_extras_delivery_title', 'Delivery')),
  '#collapsible' => '1',
  '#type' => 'fieldset',

);

$form['delivery']['uc_review_extras_delivery_show'] = array(
  '#weight' => '0',
  '#type' => 'checkbox',
  '#title' => t("Display '@title' notice", array('@title' => variable_get('uc_review_extras_delivery_title', ''))),
  '#default_value' => variable_get('uc_review_extras_delivery_show', TRUE),

  '#summary callback' => 'summarize_checkbox',
  '#summary arguments' => array(
      t("The '@title' notice is enabled.", array('@title' => variable_get('uc_review_extras_delivery_title', ''))),
      t("The '@title' notice is disabled.", array('@title' => variable_get('uc_review_extras_delivery_title', ''))),
    ),
);

$form['delivery']['uc_review_extras_delivery_title'] = array(
  '#weight' => '0',
  '#size' => '24',
  '#type' => 'textfield',
  '#title' => t('Title'),
  '#default_value' => variable_get('uc_review_extras_delivery_title', 'Delivery'),
);

$form['delivery']['uc_review_extras_delivery_text'] = array(
  '#rows' => '5',
  '#cols' => '40',
  '#weight' => '1',
  '#type' => 'textarea',
  '#title' => t('Text'),
  '#default_value' => variable_get('uc_review_extras_delivery_text', 'Delivery note here'),
);


//security

$form['security'] = array(
  '#weight' => '-8',
  '#collapsed' => '0',
  '#title' => t(variable_get('uc_review_extras_security_title', 'Security')),
  '#collapsible' => '1',
  '#type' => 'fieldset',

);

$form['security']['uc_review_extras_security_show'] = array(
  '#weight' => '0',
  '#type' => 'checkbox',
  '#title' => t("Display '@title' notice", array('@title' => variable_get('uc_review_extras_security_title', ''))),
  '#default_value' => variable_get('uc_review_extras_security_show', TRUE),

  '#summary callback' => 'summarize_checkbox',
  '#summary arguments' => array(
      t("The '@title' notice is enabled.", array('@title' => variable_get('uc_review_extras_security_title', ''))),
      t("The '@title' notice is disabled.", array('@title' => variable_get('uc_review_extras_security_title', ''))),
    ),
);

$form['security']['uc_review_extras_security_title'] = array(
  '#weight' => '0',
  '#size' => '24',
  '#type' => 'textfield',
  '#title' => t('Title'),
  '#default_value' => variable_get('uc_review_extras_security_title', 'Security'),
);

$form['security']['uc_review_extras_security_text'] = array(
  '#rows' => '5',
  '#cols' => '40',
  '#weight' => '1',
  '#type' => 'textarea',
  '#title' => t('Text'),
  '#default_value' => variable_get('uc_review_extras_security_text', 'Security note here'),
);




  return system_settings_form($form);
}

